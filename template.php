<?php

/**
 * @file
 * This file provides theme override functions for the hfccbase theme.
 */

/**
 * from ZEN: Override or insert PHPTemplate variables into the page templates.
 *
 * This function creates the body classes that are relative to each page
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("page" in this case.)
 */
function hank_preprocess_page(&$vars, $hook) {
  global $theme;

  // Don't display empty help from node_help().
  if ($vars['help'] == "<div class=\"help\"><p></p>\n</div>") {
    $vars['help'] = '';
  }

  // Let's theme the primary and secondary links here, instead of calling theme() in page.tpl.php, ok?
  if ($vars['primary_links']) {
    $vars['themed_primary_links'] = theme('links', $vars['primary_links'], array('class' => 'links primary-links'));
  }
  if ($vars['secondary_links']) {
    $vars['themed_secondary_links'] = theme('links', $vars['secondary_links'], array('class' => 'links secondary-links'));
  }

  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  $body_classes = array($vars['body_classes']);
  if (user_access('administer blocks')) {
	  $body_classes[] = 'admin';
	}
  if (!$vars['is_front']) {
    // Add unique classes for each page and website section
    $path = drupal_get_path_alias($_GET['q']);
    list($section, ) = explode('/', $path, 2);
    $body_classes[] = hank_id_safe('page-'. $path);
    $body_classes[] = hank_id_safe('section-'. $section);

    if (arg(0) == 'node') {
      if (arg(1) == 'add') {
        if ($section == 'node') {
          array_pop($body_classes); // Remove 'section-node'
        }
        $body_classes[] = 'section-node-add'; // Add 'section-node-add'
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
        if ($section == 'node') {
          array_pop($body_classes); // Remove 'section-node'
        }
        $body_classes[] = 'section-node-'. arg(2); // Add 'section-node-edit' or 'section-node-delete'
      }
    }
  }

  $vars['body_classes'] = implode(' ', $body_classes); // Concatenate with spaces
}

/**
 * from ZEN: Override or insert PHPTemplate variables into the node templates.
 *
 * This function creates the NODES classes, like 'node-unpublished' for nodes
 * that are not published, or 'node-mine' for node posted by the connected user...
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("node" in this case.)
 */
function hank_preprocess_node(&$vars, $hook) {
  global $user;

  // Special classes for nodes
  $node_classes = array();
  if ($vars['sticky']) {
    $node_classes[] = 'sticky';
  }
  if (!$vars['node']->status) {
    $node_classes[] = 'node-unpublished';
    $vars['unpublished'] = TRUE;
  }
  else {
    $vars['unpublished'] = FALSE;
  }
  if ($vars['node']->uid && $vars['node']->uid == $user->uid) {
    // Node is authored by current user
    $node_classes[] = 'node-mine';
  }
  if ($vars['teaser']) {
    // Node is displayed as teaser
    $node_classes[] = 'node-teaser';
  }
  // Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
  $node_classes[] = 'node-type-'. $vars['node']->type;
  $vars['node_classes'] = implode(' ', $node_classes); // Concatenate with spaces

  // Add Issue Number to title for Project Issue nodes.
  if ($vars['node']->type == "project_issue") {
    $vars['title'] = "Issue #" . $vars['node']->nid . ": " . $vars['title'];
  }

}

/**
 * Implementation of hook_preprocess_user_picture().
 * Taken from OpenAtrium ginkgo theme, heavily modified.
 */
function hank_preprocess_user_picture(&$vars) {
  $account = $vars['account'];
  if (isset($account->picture) && module_exists('imagecache')) {
    $attr = array('class' => 'user-picture');
    $preset = variable_get('hank_imagecache_user_picture', 'user-large');
    if ($view = views_get_current_view()) {
      // echo '<pre>'. $view->name .'</pre>';
      switch ($view->name) {
        //case 'og_members_faces':
        //case 'og_members_block':
        case 'netadmin_ron':
          $preset = 'user-thumb';
          break;
      }
    }
    if (file_exists($account->picture)) {
      $image = imagecache_create_url($preset, $account->picture);
      $alt = t($account->profile_fullname ? $account->profile_fullname : $account->name);
      $imgattr = array('class' => 'user-picture');
      $imgpath = theme('image', $image, $alt, $alt, $imgattr, FALSE);
      $linkattr = array('attributes' => array('title' => t('View user profile.')), 'html' => TRUE);
      $vars['picture'] = l($imgpath, 'user/'. $account->uid, $linkattr);
      
    }
    //print '<pre>PICTURE: '. htmlspecialchars($vars['picture']) .'</pre>';
    $vars['preset'] = $preset;
  }
}

/**
 * Create some custom classes for comments.
 */
function comment_classes($comment) {
  $node = node_load($comment->nid);
  global $user;
  $output .= ($comment->new) ? ' comment-new' : ''; 
  $output .=  ' '. $status .' '; 
  if ($node->name == $comment->name) {	
    $output .= 'node-author';
  }
  if ($user->name == $comment->name) {	
    $output .=  ' mine';
  }
  return $output;
}

/**
 * Customize the PRIMARY and SECONDARY LINKS, to allow the admin tabs to work on all browsers.
 *
 * An implementation of theme_menu_item_link().
 *
 * @param $link
 *   array The menu item to render.
 * @return
 *   string The rendered menu item.
 */
function hank_menu_item_link($link) {
  if (empty($link['options'])) {
    $link['options'] = array();
  }

  // If an item is a LOCAL TASK, render it as a tab
  if ($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="tab">'. check_plain($link['title']) .'</span>';
    $link['options']['html'] = TRUE;
  }
  else {
    $link['options']['attributes']['class'] = 'menu-'. hank_id_safe(token_replace($link['title']));
    //$link['options']['attributes']['title'] = check_plain($link['title']);
  }
            
  if (empty($link['type'])) {
    $true = TRUE;
  }

  return l($link['title'], $link['href'], $link['options']);
}

/**
 * Duplicate of theme_menu_local_tasks() but adds clear-block to tabs.
 */
function hank_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs primary clear-block\">\n". $primary ."</ul>\n";
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $output .= "<ul class=\"tabs secondary clear-block\">\n". $secondary ."</ul>\n";
  }

  return $output;
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 *   - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 *   - Replaces any character except A-Z, numbers, and underscores with dashes.
 *   - Converts entire string to lowercase.
 *
 * @param $string
 *   The string.
 * @return
 *   The converted string.
 */
function hank_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id'. $string;
  }
  return $string;
}

/**
 * Return a themed breadcrumb trail.
 *
 * Allows customization of the breadcrumb markup.
 */
function hank_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' » ', $breadcrumb) .'</div>';
  }
}

/**
 * Overrides default theme_username() function to display user profile full name
 * (if it exists) and remote "not verified" from anonymous comments.
 */
function hank_username($object) {
  if (empty($object->profile_fullname)) {
    if ($object->uid && function_exists('profile_load_profile')) {
      profile_load_profile($object);
    }
  }

  if (!empty($object->profile_fullname)) {
    $name = $object->profile_fullname;
    if (user_access('access user profiles')) {
      return l($name, 'user/'. $object->uid, array('attributes' => array('title' => t('View user profile.'))));
    } else {
      return check_plain($name);
    }
  }

  // Profile field not set, default to standard behaviour
                              
  if ($object->uid && $object->name) {
    // Shorten the name when it is too long or it will break many tables.
    if (drupal_strlen($object->name) > 20) {
      $name = drupal_substr($object->name, 0, 15) .'...';
    }
    else {
      $name = $object->name;
    }

    if (user_access('access user profiles')) {
      $output = l($name, 'user/'. $object->uid, array('attributes' => array('title' => t('View user profile.'))));
    }
    else {
      $output = check_plain($name);
    }
  }
  else if ($object->name) {
    // Sometimes modules display content composed by people who are
    // not registered members of the site (e.g. mailing list or news
    // aggregator modules). This clause enables modules to display
    // the true author of the content.
    if (!empty($object->homepage)) {
      $output = l($object->name, $object->homepage, array('attributes' => array('rel' => 'nofollow')));
    }
    else {
      $output = check_plain($object->name);
    }

    // $output .= ' ('. t('not verified') .')';
  }
  else {
    $output = variable_get('anonymous', t('Anonymous'));
  }

  return $output;
}

/**
 * Process variables for search-result.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $result
 * - $type
 *
 * @see search-result.tpl.php
 * @see search.pages.inc
 */
function hank_preprocess_search_result(&$variables) {
  $result = $variables['result'];
  $variables['url'] = check_url($result['link']);
  $variables['title'] = check_plain($result['title']);

  $info = array();
  if (!empty($result['type'])) {
    $info['type'] = check_plain($result['type']);
  }
  if (!empty($result['user'])) {
    $info['user'] = $result['user'];
  }
  if (!empty($result['date'])) {
    $info['date'] = format_date($result['date'], 'small');
  }
  if (!empty($result['node']->type) && $result['node']->type == 'project_issue' && !empty($result['node']->project_issue)) {
    $variables['title'] = t('Issue #') . $result['node']->nid . ': ' . check_plain($result['title']);
    if(!empty($info['user'])) {
      $info['user'] = t('Submitted by: ') . $info['user'];
    }
    $issue = $result['node']->project_issue;
    $info['priority'] = t('Priority: ') . project_issue_priority($issue['priority']);
    $info['status'] = t('Status: ') . project_issue_state($issue['sid']);
    if (!empty($issue['assigned']) && $account = user_load(array('uid' => $issue['assigned']))) {
      $info['assigned'] = t('Assigned: ') . hank_username($account);
    }
    // else {
    //  $info['assigned'] = t('Unassigned.');
    // }
  }
  if (isset($result['extra']) && is_array($result['extra'])) {
    $info = array_merge($info, $result['extra']);
  }
  // Check for existence. User search does not include snippets.
  $variables['snippet'] = isset($result['snippet']) ? $result['snippet'] : '';
  // Provide separated and grouped meta information..
  $variables['info_split'] = $info;
  $variables['info'] = implode(' - ', $info);
  // Provide alternate search result template.
  $variables['template_files'][] = 'search-result-'. $variables['type'];
}
